# archfy
Arch Linux configuration files.

# System

## Microcode Updates

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Microcode#Enabling_Intel_microcode_updates)
Microcode Updates gives stability and security to the processor microcode. It's highly recommended to install microcode updates. Don't forget to update GRUB.

### Intel 

```bash
sudo pacman -S intel-ucode
```
### AMD 

```bash
sudo pacman -S amd-ucode
```
## Limit journald log size

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Systemd/Journal)

Edit `/etc/systemd/journald.conf`:

- Uncomment `SystemMaxUse=` and append `200M` (or any size you like).

## Enable weekly fstrim

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Solid_state_drive)

Enable weekly TRIM to enhance SSD lifetime.

```bash
sudo systemctl enable fstrim.timer
```

## Enable insults in sudo

[Arch Wiki reference](https://wiki.archlinux.org/index.php/Sudo#Enable_insults)

Add `Defaults insults` to the end of `/etc/sudoers`

# Package Management

## Select pacman mirrors

[Arch wiki reference](https://wiki.archlinux.org/index.php/Reflector)

```bash
sudo pacman -S reflector
sudo reflector --latest 200 --protocol http --protocol https --sort rate --save /etc/pacman.d/mirrorlist
```

## Enable colors in pacman

[Arch wiki reference](https://wiki.archlinux.org/index.php/Color_output_in_console#pacman)

Edit `/etc/pacman.conf` and uncomment the row saying `Color`

## Enable parallel compilation and compression

[Arch wiki reference](https://wiki.archlinux.org/index.php/makepkg#Parallel_compilation)

Use all threads to compile and compress packages.

Edit `/etc/makepkg.conf`:

- Add the following make flag: `MAKEFLAGS="-j$(nproc)"`
- Edit the row saying `COMPRESSXZ=(xz -c -z -)` to `COMPRESSXZ=(xz -c -z - --threads=0)`
- `sudo pacman -S pigz` and edit the row saying `COMPRESSGZ=(gzip -c -f -n)` to `COMPRESSGZ=(pigz -c -f -n)`

## Building optimized binaries

[Arch wiki reference](https://wiki.archlinux.org/index.php/makepkg#Building_optimized_binaries)

Use CPU_FLAGS_X86 to build optimized binaries based on processor's architecture.

Edit `/etc/makepkg.conf`:

- Remove any `-march` and `-mtune` flags on `CFLAGS` and `CXXFLAGS`
- Add `-march=native` flag to `CFLAGS` and `CXXFLAGS`

# Networking

## Change NetworkManager's DHCP Client

[Arch wiki reference](https://wiki.archlinux.org/index.php/NetworkManager#DHCP_client)

NetworkManager's DHCP client may crash and start consumming a lot of RAM, to solve this you need to change DHCP client to `dhcpcd` or `dhclient`.

Use `dhclient` as DHCP client:

- Install `dhclient` using pacman: `sudo pacman -S dhclient`
- Set the option `main.dhcp=dhclient` with a configuration file in `/etc/NetworkManager/conf.d/`

## Use dnsmasq as DNS cache

[Arch wiki reference](https://wiki.archlinux.org/index.php/NetworkManager#DNS_caching_and_conditional_forwarding)

Install `dnsmasq` and disable `systemd-resolved`:

```bash
sudo pacman -S dnsmasq
sudo systemctl disable systemd-resolved
```

After installing `dnsmasq`, set `main.dns=dnsmasq` on `/etc/NetworkManager/conf.d/`.

# Multimedia

## Fix microphone/audio not working

Some notebooks (like DELL G3 3590) may have some audio problems, to solve this you have to update kernel to `5.6.3+` and install `sof-firmware` via pacman.